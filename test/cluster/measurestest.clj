(ns cluster.measurestest
  "Testing the functions in measure."
  (:use clojure.test
        cluster.measure
        cluster.graphexamples
        midje.sweet
        clojure.pprint)
  (:require  [clojure.set :as set]
             [clojure.math.combinatorics :as combinat]
             [clojure.core.matrix :as matrix]
             [clojure.core.matrix.operators :as matrixo])
  (:import [cluster.graphexamples weighed-graph WeighedGraphMAT]))

(def wg1
  (weighed-graph. #{1 2 3}
                  {[1 2] 1 [1 1] 1 [1 3] 1}))

(def wg2
  (weighed-graph. #{1 2 3}
                  {[1 2] 0.1 [1 1] 0.2 [1 3] 0.7}))

(def wg3
  (weighed-graph. #{1 2 3}
                  {[1 2] 0.5 [2 1] 0.5}))

(def wg4
  (weighed-graph. #{1 2 3 4}
                  {[1 2] 1 [2 1] 1 [3 4] 1 [4 3] 1}))

(def wg5
  (weighed-graph. #{1 2 3}
                  {[1 2] 1 [2 1] 1 [1 3] 1 [3 1] 1 [2 3] 1 [3 2] 1}))

(def wg6
  (weighed-graph. #{1 2 3 4}
                  {[1 1] 1 [2 2] 1 [3 3] 1 [4 4] 1}))

(def wgm1
  (WeighedGraphMAT. #{0 1 2} (matrix/identity-matrix 3)))

(facts "misc set properties"
  (tabular
   (fact "all nonempty predicate"
     (all-non-empty? ?sos) => ?value)
   ?value ?sos
   false  #{#{} #{1}}
   true   #{ #{1 2 3}}
   true   #{}
   false  #{ #{1} #{}}
   true   #{#{1} #{2}})

  (tabular
   (fact "pairwise disjoint predicate"
     (pairwise-disjoint? ?sos) => ?value)
   ?value ?sos
   true   #{#{1 2} #{3 4}}
   true   #{#{1} #{2} #{3}}
   false  #{#{1 2 3} #{3 4 5}}
   true   #{#{} #{1 2 3}}
   true   #{#{1 2}}
   true   #{#{}})
  (tabular
   (fact "partition predicate"
     (partition? ?sos) => ?value)
   ?value ?sos
   true   #{#{1} #{2} #{3}}
   true   #{#{1 2 3}}
   false  #{#{1} #{2} #{3} #{}}
   true  #{#{0} #{1} #{2} #{3}})
  (tabular
   (fact "clustering of predicate"
     (clustering-of? ?set ?clustering) => ?value)
   ?value ?set      ?clustering
   true   #{1 2 3}  #{#{1} #{2} #{3}}
   true   #{1 2 3}  #{#{1 2 3}}
   false  #{1 2 3}  #{#{1} #{2} #{3} #{}}
   false  #{1 2 3}  #{#{0} #{1} #{2} #{3}}))


(facts "testing helper functions"
  (tabular
   (fact "find cluster"
     (cluster-of ?clustering ?elt) => ?cluster)
   ?elt ?cluster ?clustering
   1    #{1 2 3} #{#{1 2 3} #{4 5 6}}
   1    #{1 2}   #{#{4 5 6} #{1 2}})
  (tabular
   (facts "same cluster"
     (same-cluster? ?clustering ?elt1 ?elt2) => ?value)
   ?value ?elt1 ?elt2 ?clustering
   true   1     2    #{#{1 2} #{3 4}}
   false  1     2    #{#{1} #{2}})
  (tabular
   (facts "sum of weights"
     (sum-of-weights ?wgraph ?elt) => ?value)
   ?value ?wgraph ?elt
   3      wg1     1
   1.0    wg2     1)
  (tabular
   (facts "sum of all weights"
     (sum-of-all-weights ?wgraph) => ?value)
   ?value ?wgraph
   3      wg1
   1.0    wg2
   3.0    wgm1))


(facts "modularity computations"
  (tabular
   (fact "modularity scalar"
     (modularity-scalar ?wgraph) => ?value)
   ?value ?wgraph
   1/3    wg1
   1.0    wg2
   1.0    wg3
   1/6    wg5
   1/4    wg6)
  (tabular
   (fact "modularity term"
     (modularity-term ?wgraph ?clustering 1 2) => ?value)
   ?value                     ?wgraph ?clustering
   0                          wg1     #{#{1} #{2} #{3}}
   1                          wg1     #{#{1 2} #{3}}
   (- 0.5 (* 1.0  0.25))      wg3     #{#{1 2} #{3}})
  (tabular
   (fact "modularity"
     (modularity ?wgraph ?clustering) => ?value)
   ?value   ?wgraph ?clustering
   1/2      wg4     #{#{1 2} #{3 4}}
   -1/2     wg4     #{#{1 3} #{2 4}}
   0        wg6     #{#{1 2 3 4}}
   (/ -2 9) wg5     #{#{1 2} #{3}}
   0        wg5     #{#{1 2 3}})
  (tabular
   (fact "delta-modularity-related (sigma-in)"
     (sigma-in ?wgraph ?cluster) => ?value)
   ?value ?wgraph ?cluster
   2      wg4     #{1 2}
   1      wg1     #{1}
   0      wg4     #{1 4}
   2      wg5     #{1 2}
   0      wg5     #{1})
  (tabular
   (fact "sigma-tot values"
     (sigma-tot ?wgraph ?cluster) => ?value)
   ?value ?wgraph ?cluster
   2      wg4     #{1 2}
   3      wg1     #{1}
   2      wg4     #{1 4}
   4      wg5     #{1 2})
  (tabular
   (facts "delta-modularity itself"
     (delta-modularity ?wgraph ?cluster ?elt) => ?value)
   ?value       ?wgraph ?cluster ?elt
   (/ 2 9)      wg5     #{1 2}   3))

(facts "modularity computations on different representations"
  (let [mclust4       (createMatrixClusterable (getMATFlex wg4))
        mclust3       (createMatrixClusterable (getMATFlex wg3))
        mclust2       (createMatrixClusterable (getMATFlex wg2))]
    (fact "modularity scalar is the same"
      (:modularityscalar mclust4) => (double (modularity-scalar wg4))
      (:modularityscalar mclust3) => (double (modularity-scalar wg3))
      (:modularityscalar mclust2) => (double (modularity-scalar wg2)))
    (fact "delta modularity is the same"
      (deltamodularity mclust4 2 1) => (double (delta-modularity wg4 #{2} 1))
      (deltamodularity mclust4 3 1) => (double (delta-modularity wg4 #{3} 1))
      (deltamodularity mclust3 2 1) => (double (delta-modularity wg3 #{2} 1)))))

(facts "matrix collapse pts works"
  (let [mclust4       (createMatrixClusterable (getMATFlex wg4))
        mclust3       (createMatrixClusterable (getMATFlex wg3))
        mclust2       (createMatrixClusterable (getMATFlex wg2))
        collwg4       (collapse-nodes wg4 2 3)
        collmclust4   (collapsenodes mclust4 2 3)
        mclustcoll4   (createMatrixClusterable (getMATFlex collwg4))
        c4v           (first (set/difference (vertices collmclust4)
                                             (vertices mclustcoll4)))
        m4v           (first (set/difference (vertices mclustcoll4)
                                             (vertices collmclust4)))]
    (fact "have the same number of vertices"
      (count (vertices collmclust4)) => (count (vertices mclustcoll4)))
    (fact "values agree"
      (edgewt collmclust4 4 c4v) => (edgewt mclustcoll4 4 m4v)
      (edgewt collmclust4 1 c4v) => (edgewt mclustcoll4 1 m4v)
      (edgewt collmclust4 c4v 4) => (edgewt mclustcoll4 m4v 4)
      (edgewt collmclust4 c4v 1) => (edgewt mclustcoll4 m4v 1)
      (edgewt collmclust4 c4v c4v) => (edgewt mclustcoll4 m4v m4v))))


(facts "fast clustering gives expected result"
  (let [K     (scaled-K 5 1)
        KK    (disjoint-union K K)
        mcKK  (createMatrixClusterable (getMATFlex KK))
        vert  (vertices mcKK)]
    (:clustermap (takestep mcKK "A-0"))
    => {1 {"A-0" #{"A-0" "A-4"}, "B-0" #{"B-0"}, "A-1" #{"A-1"}, "B-1" #{"B-1"},
           "A-2" #{"A-2"}, "B-2" #{"B-2"}, "A-3" #{"A-3"}, "B-3" #{"B-3"},
           "B-4" #{"B-4"}},
        0 {"A-0" #{"A-0"}, "B-0" #{"B-0"}, "A-1" #{"A-1"}, "B-1" #{"B-1"},
           "A-2" #{"A-2"}, "B-2" #{"B-2"}, "A-3" #{"A-3"}, "B-3" #{"B-3"},
           "A-4" #{"A-4"}, "B-4" #{"B-4"}}}
    (:clustermap (takestep (takestep mcKK "A-0") "A-0"))
    => { 2
        {"A-0" #{"A-0" "A-2" "A-3"},
         "B-0" #{"B-0"},
         "A-1" #{"A-1"},
         "B-1" #{"B-1"},
         "B-2" #{"B-2"},
         "B-3" #{"B-3"},
         "A-4" #{"A-4"},
         "B-4" #{"B-4"}},
        1
        {"A-0" #{"A-0" "A-3"},
         "B-0" #{"B-0"},
         "A-1" #{"A-1"},
         "B-1" #{"B-1"},
         "A-2" #{"A-2"},
         "B-2" #{"B-2"},
         "B-3" #{"B-3"},
         "A-4" #{"A-4"},
         "B-4" #{"B-4"}},
        0
        {"A-0" #{"A-0"},
         "B-0" #{"B-0"},
         "A-1" #{"A-1"},
         "B-1" #{"B-1"},
         "A-2" #{"A-2"},
         "B-2" #{"B-2"},
         "A-3" #{"A-3"},
         "B-3" #{"B-3"},
         "A-4" #{"A-4"},
         "B-4" #{"B-4"}}}
    (:clustermap (takestep (takestep (takestep mcKK "A-0") "A-0") "A-0")) =>
    {3
     {"A-0" #{"A-0" "A-1" "A-3" "A-4"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "A-2" #{"A-2"},
      "B-2" #{"B-2"},
      "B-3" #{"B-3"},
      "B-4" #{"B-4"}},
     2
     {"A-0" #{"A-0" "A-1" "A-4"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "A-2" #{"A-2"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "B-4" #{"B-4"}},
     1
     {"A-0" #{"A-0" "A-1"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "A-2" #{"A-2"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "A-4" #{"A-4"},
      "B-4" #{"B-4"}},
     0
     {"A-0" #{"A-0"},
      "B-0" #{"B-0"},
      "A-1" #{"A-1"},
      "B-1" #{"B-1"},
      "A-2" #{"A-2"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "A-4" #{"A-4"},
      "B-4" #{"B-4"}}}
    (:clustermap (takestep (takestep (takestep (takestep mcKK "A-0") "A-0") "A-0") "A-0")) =>
    {4
     {"A-0" #{"A-0" "A-1" "A-2" "A-3" "A-4"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "B-2" #{"B-2"},
      "B-3" #{"B-3"},
      "B-4" #{"B-4"}},
     3
     {"A-0" #{"A-0" "A-1" "A-2" "A-4"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "B-4" #{"B-4"}},
     2
     {"A-0" #{"A-0" "A-1" "A-2"},
      "B-0" #{"B-0"},
      "B-1" #{"B-1"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "A-4" #{"A-4"},
      "B-4" #{"B-4"}},
     1
     {"A-0" #{"A-0" "A-2"},
      "B-0" #{"B-0"},
      "A-1" #{"A-1"},
      "B-1" #{"B-1"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "A-4" #{"A-4"},
      "B-4" #{"B-4"}},
     0
     {"A-0" #{"A-0"},
      "B-0" #{"B-0"},
      "A-1" #{"A-1"},
      "B-1" #{"B-1"},
      "A-2" #{"A-2"},
      "B-2" #{"B-2"},
      "A-3" #{"A-3"},
      "B-3" #{"B-3"},
      "A-4" #{"A-4"},
      "B-4" #{"B-4"}}}))

(facts "delta-modularity gets same values on both representations"
  (tabular (delta-modularity ?wgraph #{?elt-c} ?elt-add) => (delta-modularity-nbd ?nbdgraph ?elt-c ?elt-add)
           ?wgraph ?elt-c ?elt-add ?nbdgraph
           wg4     1      2        (getNBDrep wg4)
           wg5     1      2        (getNBDrep wg5)
           wg2     1      2        (getNBDrep wg2)
           wg2     2      1        (getNBDrep wg2)))

(facts "commuting diagram of collapsing and using delta"
  (let [K                     (scaled-K 5 1)
        Kc                    (collapse-nodes K 0 1)
        modularity-before     (modularity K (map #(set [%]) (vertices K)))
        modularity-after      (modularity Kc (map #(set [%]) (vertices Kc)))
        delta-modularity      (delta-modularity K #{0} 1)]
    (+ modularity-before delta-modularity) => modularity-after))

(facts "commuting with delta, collapsing, and new representation"
  (let [K                     (scaled-K 5 1)
        KK                    (disjoint-union K K)
        KKnbd                 (getNBDrep KK)
        KKc                   (collapse-nodes KK "A-0" "A-4")
        modularity-before     (modularity KK (map #(set [%]) (:vertices KK)))
        modularity-after      (modularity KKc (map #(set [%]) (:vertices KKc)))
        delta-modularity      (delta-modularity KK #{"A-0"} "A-4")
        delta-modularity-nbd  (delta-modularity-nbd KKnbd "A-0" "A-4")]
    (fact "delta" (+ modularity-before delta-modularity) => modularity-after)
    (fact "two deltas" delta-modularity => delta-modularity-nbd)))


(facts "size statistics work"
  (cluster-size-statistics [[1] [1 1] [1 1 1] [1 1 1 1] [1 1 1 1 1 1]]) =>
  {:number 5 :min 1 :max 6 :mean (double (/ 16 5)), :spread 5, :quartiles [1, 2, 3]}

  (cluster-size-statistics (map #(repeat % 1) (range 1 13))) =>
  {:number 12 :min 1 :max 12 :mean 6.5, :spread 11, :quartiles [3 6 9]}

  (min-similarity [0 1 2] {[0 1] 1 [0 2] 1 [1 2] 0.1}) =>
  0.1

  (min-similarity [0 1 2] {[0 1] 1 [0 2] 1 [1 2] 1.1}) =>
  1

  (min-similarity [1 2 3] {[1 2] 0.1 [2 3] 0.2 [1 3] 0.2}) =>
  0.1

  (tabular
   (fact (min-similarity ?cluster ?similarity) => ?result)
   ?cluster   ?similarity                       ?result
   [0 1 2]    {[0 1] 1 [0 2] 1 [1 2] 0.1}       0.1
   [0 1 2]    {[0 1] 1 [0 2] 1 [1 2] 1.1}       1
   [1 2 3]    {[1 2] 0.1 [2 3] 0.2 [1 3] 0.2}   0.1
   [1 2 3]    {[1 2] 0.1 [2 3] :NaN [1 3] 0.2}  :NaN)
  (tabular
   (min-similarity ?cluster ?similarity :dropones) => ?result
   ?cluster   ?similarity                       ?result
   [0 1 2]    {[0 1] 1 [0 2] 1 [1 2] 0.1}       0.1
   [0 1 2]    {[0 1] 1 [0 2] 1 [1 2] 1.1}       :NaN
   [1 2 3]    {[1 2] 0.1 [2 3] 0.2 [1 3] 0.2}   0.1)

  (max-inter-cluster-similarity [0 1] [2 3] {[0 2] 1 [0 3] 2 [1 2] 3 [1 3] 4}) =>
  4

  (max-inter-cluster-similarity [0 1] [2 3] {[0 2] 1 [0 3] 2 [1 2] 3 [1 3] 0.4}) =>
  3

  (min-cluster-similarity [[1] [2] [3] [4]] {}) =>
  :NaN

  (min-cluster-similarity [[1 2] [3] [4]] {[1 2] 3.0}) =>
  3.0

  (max-inter-cluster-similarity [1] [2] {[1 2] 3.0}) =>
  3.0

  (max-inter-cluster-similarity [1 2] [3 4] {[1 3] 3.0 [1 4] 2.0 [2 3] 2.0 [2 4] 2.0}) =>
  3.0

  (cluster-diameter [1 2 3] {[1 2] 0.1 [2 3] 0.2 [1 3] 0.1}) =>
  0.9

  (cluster-distance [1 2] [3 4]  {[1 3] 0.3 [1 4] 0.2 [2 3] 0.2 [2 4] 0.2} ) =>
  0.7
  )
