(ns cluster.hierarchicaltest
  "Test the hierarchical clustering."
  (:require [cluster.similarity :as similarity]
            [schema.core :as s])
  (:use cluster.core
        cluster.hierarchical
        midje.sweet))

(background (around :facts (s/with-fn-validation ?form)))

(fact "do some hierarchical clusterings"
  (cluster {:method :hierarchical
            :similarity (similarity/set-of-numbers #{1 2 4 5})
            :loop-function (create-loop-function)})
  =>
  {1 #{#{1} #{2} #{4} #{5}}, 2 #{#{1} #{2} #{4 5}}, 3 #{#{1 2} #{4 5}}, 4 #{#{1 2 4 5}}}

  (cluster {:method :hierarchical
            :similarity (similarity/set-of-numbers #{1 2 4 7})
            :loop-function (create-loop-function)})
  =>
  {1 #{#{1} #{2} #{4} #{7}}, 2 #{#{1 2} #{4} #{7}}, 3 #{#{7} #{1 2 4}}, 4 #{#{1 2 4 7}}})
