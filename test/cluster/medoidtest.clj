(ns cluster.medoidtest
  "Test the medoid clustering.

   The first part tests all functions used for the clustering, the final
   part checks some actual clustering examples."
  (:require [cluster.similarity :as similarity]
            [schema.core :as s])
  (:use cluster.core
        cluster.medoid
        midje.sweet))

(background (around :facts (s/with-fn-validation ?form)))

(def sons1  (similarity/set-of-numbers #{1 2 3}))
(def sons2  (similarity/set-of-numbers #{1.0 1.1 0.9 2.1 2 1.9 3.0 3.1 2.9}))

(facts "k-medoid helper functions"
  (let [n  2
        s  #{1 2 3 4 5 6 7}
        m  (pick-medoids (seq s) n)]
    (fact (count m) => n)
    (fact m => (has every? s))))

(tabular
 (facts "picking medoids gets the right number of distinct elements from input"
   (count (set (pick-medoids (seq ?s) ?n))) => ?m
   (pick-medoids (seq ?s) ?n) => (has every? ?s))
 ?s               ?n ?m
 #{1 2 3 4 5 6 7} 1  1
 #{1 2 3 4 5 6 7} 2  2
 #{1 2 3 4 5 6 7} 6  6
 #{1 2 3 4 5 6 7} 7  7
 #{1 2 3 4 5 6 7} 8  7  ;; no more available so take all
 #{1 2 3 4 5 6 7} 81 7  ;; no more available so take all
 )

(tabular
 (fact "get-location-maximum gets the location of the maximum"
   (get-location-maximum ?pts ?fn) => ?max-loc)
 ?pts        ?fn            ?max-loc
 [1 2]       identity       2
 [1 2]       (comp - identity) 1
 [-1 0 1 2]  #(* % %)       2
 [-2 -1 0 1] #(* % %)      -2)


(let [sp1    (similarity/set-of-numbers #{1 2 3})
       sp2   (similarity/set-of-numbers #{1.0 1.1 1.2})]
  (tabular
   (fact "picking closest medoid"
     (closest-medoid ?pt ?meds ?sim) => ?closest)
   ?pt ?meds     ?sim ?closest
   1   [1]       sp1  1
   1   [2]       sp1  2
   1   [1 2]     sp1  1
   1   [2 3]     sp1  2
   1.0 [1.1 1.2] sp2  1.1
   1.1 [1.2]     sp2  1.2))

(tabular
 (fact "checking map-choice"
   (map-choice ?pts ?map) => ?res)
 ?pts   ?map              ?res
 [1 2]  {1 [1 2] 2 [1 2]} true
 [1]    {1 [2]}           false)

(let [sp1   (similarity/set-of-numbers #{1 2 3})
      sp2   (similarity/set-of-numbers #{1.0 1.1 1.2 1.3 1.4})]
  (tabular
   (fact (medoid-clusters-h {:points ?pts :medoids ?med :similarity ?sim :clumap ?clumap})
     =>
     ?clust)
   ?pts                  ?med      ?sim   ?clumap ?clust
   [1 2]                 [1]       sp1    {}      {1 [1 2]}
   [1 2 3]               [1 2]     sp1    {}      {1 [1] 2 [2 3]}
   [1.0 1.1 1.2 1.3 1.4] [1.1 1.4] sp2    {}      {1.1 [1.0 1.1 1.2], 1.4 [1.3 1.4]}))

(tabular
 (fact "pairwise disjoint"
   (pairwise-disjoint ?ss) => ?res)
 ?res ?ss
 true    [#{1 2} #{3 4}]
 false    [#{1 2} #{2 3 4}])

(let [sp1   (similarity/set-of-numbers #{1 2 3})
      sp2   (similarity/set-of-numbers #{1.0 1.1 1.2 1.3 1.4})]
  (tabular
   (fact "medoids -> clusters"
     (map sort (sort (medoid-clusters ?sim ?pts  ?med)))
     =>
     (map sort (sort ?clust)))
   ?pts                  ?med      ?sim   ?clust
   [1 2]                 [1]       sp1    [[1 2]]
   [1 2 3]               [1 2]     sp1    [[1] [2 3]]
   [1.0 1.1 1.2 1.3 1.4] [1.1 1.4] sp2    [[1.0 1.1 1.2]  [1.3 1.4]]))

(let [sp1   (similarity/set-of-numbers #{1 2 3})
      sp2   (similarity/set-of-numbers #{1.0 1.1 1.2 1.3 1.4})]
  (tabular
   (fact "compute sum of similarities"
     (sum-of-similarities {:point ?pt :cluster ?cluster :similarity ?sim})
     =>
     ?res)
   ?pt  ?cluster   ?sim   ?res
   1    [2 3]      sp1    1
   1    [3]        sp1    0
   1    [2]        sp1    1
   1.0  [1.1 1.2]  sp2    (roughly 0.5 0.01)))

(let [sp1   (similarity/set-of-numbers #{1 2 3})
      sp2   (similarity/set-of-numbers #{1.0 1.1 1.2 1.3 1.4})]
  (tabular
   (fact "find medoid"
     (find-medoid {:cluster ?cluster :similarity ?sim})
     =>
     ?res)
   ?cluster       ?sim   ?res
   [2 3]          sp1    #(or (= % 2) (= % 3))
   [3]            sp1    3
   [2]            sp1    2
   [1.1 1.2]      sp2    #(or (= % 1.1) (= % 1.2))
   [1.0 1.1 1.2]  sp2    1.1))

(tabular
 (fact "set-choice check was choice"
   (set-choice ?pts ?sets) => ?res)
 ?pts      ?sets            ?res
 [1 2]     [#{1} #{2}]      true
 [1 2]     [#{1 2}]         false
 [1 2]     [#{1 3} #{4 2}]  true
 [1 2]     [#{1 3} #{4}]    false
 [1 2]     [#{1 3} #{4} #{2}] false)


(let [sp1   (similarity/set-of-numbers #{1 2 3})
      sp2   (similarity/set-of-numbers #{1.0 1.1 1.2 1.3 1.4})]
 (tabular
   (fact "find medoids"
     (find-new-medoids ?sim ?clust) => ?res)
   ?sim   ?clust          ?res
   sp1    [[1]]           [1]
   sp1    [[1 2 3]]       [2]
   sp2    [[1.0 1.1 1.4]] [1.1]))

(facts "run medoid clustering"
  (cluster {:method :k-medoid
            :similarity (similarity/set-of-numbers #{1 2 3 4 5 6 7 8 9})
            :medoid-number 1
            :iteration-no 2})
  => #{5}

  (cluster {:method :k-medoid
            :similarity (similarity/set-of-numbers #{1 2 3 7 8 9})
            :medoid-number 2
            :iteration-no 2})
  => #{2 8}

  (cluster {:method :k-medoid
            :similarity (similarity/set-of-numbers #{1 2 3 7 8 9 12 13 14})
            :medoid-number 3
            :iteration-no 7})
  => #{2 8 13}

  (cluster {:method :k-medoid
            :similarity (similarity/set-of-numbers #{1 2 3 4 5 6})
            :medoid-number 2
            :iteration-no 3})
  => #{2 5})
