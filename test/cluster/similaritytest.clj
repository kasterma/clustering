(ns cluster.similaritytest
  (:require [cluster.similarity :refer :all])
  (:use midje.sweet))

(facts "set of numbers similarity works"
  ;; Note: in these tests we test the implementation, there are uses
  ;; of sons that don't make good sense (computing sims outside of the
  ;; set).  Hence e.g. the last similarity is negative.
  (let [sons  (set-of-numbers #{1 2 3})]
    (tabular
     (fact (get-sim sons ?x ?y) => ?sim)
     ?x  ?y  ?sim
     1   2   1
     1.2 1.3 (roughly (- 2 0.1) 0.001))

    (tabular
     (fact (get-sim-pt-set sons ?x ?S) => ?sim)
     ?x  ?S             ?sim
     1   #{2 3 4}       1
     1   #{1.6 1.7 1.3} (roughly (- 2 0.3) 0.001))

    (tabular
     (fact (get-sim-set-set sons ?X ?Y) => ?sim)
     ?X       ?Y         ?sim
     #{1 2 3} #{4 5 6}   1
     #{1 2}   #{-4 -3.5} (roughly (- 2 4.5) 0.001))

    (fact (points sons) => #{1 2 3})))
