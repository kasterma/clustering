(ns cluster.graphexamplestest
  (:import [cluster.graphexamples
            weighed-graph WeighedGraphMAT WeighedGraphMATFlex])
  (:use clojure.test
        cluster.measure
        cluster.graphexamples
        midje.sweet
        clojure.pprint)
  (:require  [clojure.set :as set]
             [clojure.math.combinatorics :as combinat]
             [taoensso.timbre :as timbre
              :only (trace debug info warn error fatal spy)]
             [taoensso.timbre.profiling :as profiling :only (p profile)]
             [clojure.core.matrix :as matrix]
             [clojure.core.matrix.operators :as matrixo]))


(facts "edges-out->nbd-rep"
       (edges-out->nbd-rep [[1 2] [1 3]] {[1 2] 3 [1 3] 4}) => {2 3, 3 4}
       (edges-out->nbd-rep [] {}) => {})


(facts "weighed graph predicate and cleaning work as advertised"
  (weighed-graph? (weighed-graph. #{1 2 3} {})) => true
  (weighed-graph? (weighed-graph. #{1 2 3} #{})) => false ;; not a map
  (weighed-graph? (weighed-graph. {} {})) => false ;; not a set of vert
  (weighed-graph? (weighed-graph. #{1 2} {[2 3] 1})) => false ;; 3 not a vert
  (weighed-graph? (clean-weighed-graph (weighed-graph. #{1 2 3} #{}))) => true
  (weighed-graph? (clean-weighed-graph (weighed-graph. {} {}))) => true
  (weighed-graph? (clean-weighed-graph (weighed-graph. #{1 2} {[2 3] 1}))) => true)


(facts "weighed graph matrix flex works as advertised"
  (let [wgmf    (WeighedGraphMATFlex. #{:a :b :c} {:a 0 :b 1 :c 2} (matrix/matrix [[1 0 0] [1 1 1] [0 2 2]]))]
    (fact "vertices correctly extracted"
      (vertices wgmf) => #{:a :b :c})
    (fact "correct edge weights"
      (edgewt wgmf :a :b) => 0.0
      (edgewt wgmf :b :a) => 1.0
      (edgewt wgmf :c :b) => 2.0)
    (fact "correct sum of all weights"
      (sumofallweights wgmf) => 8.0)))

(facts "conversion to WeighedGraphMATFlex tested"
  (let [K51    (scaled-K 5 1)
        wgmf   (getMATFlex K51)]
    (fact "vertices correct"
      (vertices wgmf) => #{0 1 2 3 4})
    (tabular
     (fact "correct edge weights"
       (edgewt wgmf ?v1 ?v2) => (double (edgewt K51 ?v1 ?v2)))
     ?v1   ?v2
     0     0
     0     1
     1     0
     0     4)))

(facts "test the WeighedGraphMATFlex predicate"
  (WeighedGraphMATFlex?
   (WeighedGraphMATFlex. #{1 2 3}
                         {1 0 2 1 3 2}
                         (matrix/matrix [[1 1 1] [1 1 1] [1 1 1]])))
  => true

  (WeighedGraphMATFlex?
   (WeighedGraphMATFlex. #{1 2 3}
                         {1 0 2 1}
                         (matrix/matrix [[1 1 1] [1 1 1] [1 1 1]])))
  => false

  (WeighedGraphMATFlex?
   (WeighedGraphMATFlex. #{1 2}
                         {1 0 2 1}
                         (matrix/matrix [[1 1 1] [1 1 1] [1 1 1]])))
  => false

  (WeighedGraphMATFlex?
   (WeighedGraphMATFlex. #{1 2}
                         {1 0 2 1}
                         (matrix/matrix [[1 1 0] [1 1 0] [0 0 0]])))
  => true)

(facts "clean works on the non-zero error above"
  (WeighedGraphMATFlex?
   (clean-WeighedGraphMATFlex
    (WeighedGraphMATFlex. #{1 2}
                          {1 0 2 1}
                          (matrix/matrix [[1 1 1] [1 1 1] [1 1 1]]))))
  => false
)

(facts "circular graph"
  (tabular
   (fact "first of ith clique"
     (circular-first-ith-clique ?size ?i) => ?value)
   ?size ?i ?value
   5     0  0
   5     1  5
   481   0  0
   481   2  962)
  (tabular
   (fact "last of ith clique"
     (circular-last-ith-clique ?size ?i) => ?value)
   ?size ?i ?value
   5     0  4
   5     1  9
   481   0  480
   481   2  1442)
  (tabular
   (fact "get ith clique"
     (circular-ith-clique ?size ?i) => ?value)
   ?size ?i ?value
   5     0  '(0 1 2 3 4)
   5     1  '(5 6 7 8 9)
   481   0  (range 0 481)
   481   2  (range 962 1443))
  (let [C32 (circular 3 2)]
    (fact "circular vertices"
      (vertices C32) => #{0 1 2 3 4 5})
    (tabular
     (fact "circular edge weights"
       (edgewt C32 ?elt1 ?elt2) => ?value)
     ?elt1 ?elt2 ?value
     3 2 1
     5 4 1
     1 0 1
     2 2 1
     3 3 1
     4 4 1
     5 5 1
     0 0 1
     1 1 1
     2 3 1
     3 4 1
     4 5 1
     0 1 1
     1 2 1
     5 0 1))
)

(facts "correct conversion to NBDrep"
  (let [wg1    (weighed-graph. #{1 2 3} {[1 2] 3 [2 3] 4 [2 1] 1})
        nbd1   (getNBDrep wg1)]
    (fact "correct vertex set"
      (vertices nbd1) => #{1 2 3})
    (tabular
     (edgewt nbd1 ?v1 ?v2) => ?wt
     ?v1 ?v2 ?wt
     1   2   3
     1   1   0
     2   3   4
     3   2   0
     2   1   1)))


(facts "conversion to nbd form"
  (let [K51      (scaled-K 5 1)
        nbdK51   (getNBDrep K51)
        B34      (full-bipartite 3 4 0.5)
        nbdB34   (getNBDrep B34)
        ]
    (timbre/info :B34 B34)
    (timbre/info :nbdB34 nbdB34)
    (fact "vertex set is unchanged"
      (:vertices nbdK51) => (:vertices K51)
      (:vertices nbdB34) => (:vertices B34))
    (fact "correct value with visually inspected example - I"
      nbdK51 => {:vertices #{0 1 2 3 4},
                 :nbdmap {4 {:edges {2 1, 1 1, 0 1, 3 1}},
                          3 {:edges {1 1, 0 1, 4 1, 2 1}},
                          2 {:edges {0 1, 4 1, 3 1, 1 1}},
                          1 {:edges {4 1, 3 1, 2 1, 0 1}},
                          0 {:edges {4 1, 3 1, 2 1, 1 1}}}})
    (fact "correct value with visually inspected example - II"
      nbdB34 => {:vertices #{0 1 2 3 4 5 7},
                 :nbdmap {7 {:edges {4 0.5, 2 0.5, 0 0.5}},
                          5 {:edges {2 0.5, 0 0.5, 4 0.5}},
                          4 {:edges {1 0.5, 7 0.5, 5 0.5, 3 0.5}},
                          3 {:edges {0 0.5, 4 0.5, 2 0.5}},
                          2 {:edges {7 0.5, 5 0.5, 3 0.5, 1 0.5}},
                          1 {:edges {4 0.5, 2 0.5, 0 0.5}},
                          0 {:edges {7 0.5, 5 0.5, 3 0.5, 1 0.5}}}})))

(facts "collapse nodes nbd representation"
  (let [K51     (scaled-K 5 0.75)
        nbdK51  (getNBDrep K51)
        colK51  (collapse-nodes-nbd-representation nbdK51 0 1)]
    (fact "correct value with visually inspected example"
      colK51 =>
      (just {:vertices #{0 2 3 4},
             :nbdmap (just {0 {:edges {0 1.5, 4 1.5, 3 1.5, 2 1.5, 1 0}},
                            1 anything ; this is the removed node (stuff will be left around, don't care about values)
                            2 {:edges {0 1.5, 4 0.75, 3 0.75, 1 0}},
                            3 {:edges {1 0, 0 1.5, 4 0.75, 2 0.75}},
                            4 {:edges {2 0.75, 1 0, 0 1.5, 3 0.75}}})}))))

(facts "collapse nodes on weighed-graph representation"
  (facts "fresh-node-name"
    (is (not (contains? #{1 2} (find-fresh-node-name (weighed-graph. #{1 2} {}) (str 1 2)))))
    (is (not (contains? #{1 2} (find-fresh-node-name (weighed-graph. #{1 2} {}) 2))))
    (is (not (contains? #{1 2} (find-fresh-node-name (weighed-graph. #{1 2} {}) (str 1))))))

  (facts "actual collapse"
    (let [wg        (weighed-graph. #{1 2 3} {[1 2] 1 [1 3] 1})
          new-node  (find-fresh-node-name wg (str 2 3)) ; This is leaking implementation!!!!!
          new-wg    (collapse-nodes wg 2 3)]
      (tabular (fact ((:edgeweightsmap new-wg) [?n1 ?n2] 0) => ?new-wt)
               ?n1      ?n2       ?new-wt
               1        1         0
               1        new-node  2
               new-node 1         0
               new-node new-node  0)
      ;; in particular the name of the new node is not one of the two replaced nodes
      (fact (count (set/difference (:vertices wg) (:vertices new-wg))) => 2)
      (fact (count (set/difference (:vertices new-wg) (:vertices wg))) => 1)
      )
    (let [wg        (weighed-graph. #{1 2 3} {[1 2] 1 [1 3] 1 [3 3] 1})
          new-node  (find-fresh-node-name wg (str 2 3)) ; This is leaking implementation!!!!!
          new-wg    (collapse-nodes wg 2 3)]
      (tabular (fact ((:edgeweightsmap new-wg) [?n1 ?n2] 0) => ?new-wt)
               ?n1      ?n2       ?new-wt
               1        1         0
               1        new-node  2
               new-node 1         0
               new-node new-node  1)
      ;; in particular the name of the new node is not one of the two replaced nodes
      (fact (count (set/difference (:vertices wg) (:vertices new-wg))) => 2)
      (fact (count (set/difference (:vertices new-wg) (:vertices wg))) => 1)
      )
    (let [wg        (weighed-graph. #{1 2 3} {[1 1] 1 [1 2] 1 [1 3] 1 [2 1] 1 [3 3] 1 [2 2] 1})
          new-node  (find-fresh-node-name wg (str 2 3)) ; This is leaking implementation!!!!!
          new-wg    (collapse-nodes wg 2 3)
          new-wg-k  (collapse-nodes wg 2 3 :keep-edges)]
      (tabular
       (fact ((:edgeweightsmap ?wg) [?n1 ?n2] 0) => ?new-wt)
       ?wg      ?n1      ?n2       ?new-wt
       new-wg   1        1         1
       new-wg   1        new-node  2
       new-wg   new-node 1         1
       new-wg   new-node new-node  2
       new-wg-k 1        1         1
       new-wg-k 1        new-node  2
       new-wg-k new-node 1         1
       new-wg-k new-node new-node  2)
      ;; in particular the name of the new node is not one of the two replaced nodes
      (fact (count (set/difference (:vertices wg) (:vertices new-wg))) => 2)
      (fact (count (set/difference (:vertices new-wg) (:vertices wg))) => 1)
      (fact (count (set/difference (:vertices wg) (:vertices new-wg-k))) => 2)
      (fact (count (set/difference (:vertices new-wg-k) (:vertices wg))) => 1)
      (fact "keep edges kept the old edges around - I"
        (count (set/difference (set (keys (:edgeweightsmap wg)))
                               (set (keys (:edgeweightsmap new-wg-k))))) => 0)
      (fact "if not keeping edges they are indeed removed - I"
        (count (set/difference (set (keys (:edgeweightsmap wg)))
                               (set (keys (:edgeweightsmap new-wg))))) => 5))
    (let [wg        (weighed-graph. #{1 2 3} {})
          new-node  (find-fresh-node-name wg (str 2 3)) ; This is leaking implementation!!!!!
          new-wg    (collapse-nodes wg 2 3)
          new-wg-k  (collapse-nodes wg 2 3 :keep-edges)]
      (tabular (fact ((:edgeweightsmap ?wg) [?n1 ?n2] 0) => ?new-wt)
               ?wg      ?n1      ?n2      ?new-wt
               new-wg   1        1         0
               new-wg   1        new-node  0
               new-wg   new-node 1         0
               new-wg   new-node new-node  0
               new-wg-k 1        1         0
               new-wg-k 1        new-node  0
               new-wg-k new-node 1         0
               new-wg-k new-node new-node  0)
      ;; in particular the name of the new node is not one of the two replaced nodes
      (fact "keep edges kept the old edges around"
        (count (set/difference (:edgeweightsmap wg)
                               (:edgeweightsmap new-wg-k))) => 0)
      (fact "if not keeping edges they are indeed removed"
        (count (set/difference (:edgeweightsmap wg)
                               (:edgeweightsmap new-wg))) => 0)
      (fact (count (set/difference (:vertices wg)
                                   (:vertices new-wg))) => 2)
      (fact (count (set/difference (:vertices wg)
                                   (:vertices new-wg-k))) => 2)
      (fact (count (set/difference (:vertices new-wg)
                                   (:vertices wg))) => 1)
      (fact (count (set/difference (:vertices new-wg-k)
                                   (:vertices wg))) => 1))))

(facts "basic use of collapse-nodes-matrix-representation"
  (let [res (collapse-nodes-matrix-representation
             {:vertices->idx {":0" 0 ":1" 1}
              :vertex-cluster-map {":0" #{:a :b :c} ":1"  #{:e :f :g}}
              :rowsums {0 2 1 3}
              :modularity-scalar 5
              :nbdmatrix (matrix/matrix [[1 1] [1 1]])}
             ":0" ":1" 0.5)
        mat  (:nbdmatrix res)]
    res => (just {:vertices->idx {":0" 0},
                  :vertex-cluster-map {":0" #{:a :c :b :f :g :e}},
                  :rowsums {0 5, 1 3},
                  :modularity-scalar 1.4285714285714286,
                  :nbdmatrix anything})
    (matrix/mget mat 0 0) => 4.0))

(facts "helper functions for collapse nodes nbd-representation"
  (fact "combining"
    (@#'cluster.graphexamples/combine {1 2, 3 4, 5 6} 1 3)
    => {1 6, 3 0, 5 6}

    (@#'cluster.graphexamples/combine {1 1, 2 2, 3 3, 4 4} 3 2)
    => {1 1, 2 0, 3 5, 4 4})

  (facts "combine-wrapper"
    (@#'cluster.graphexamples/combine-wrapper
     1 2 [3 {:edges {1 1, 2 2, 3 3, 4 4} :other :otherval}]) =>
    {3 {:edges {1 3, 2 0, 3 3, 4 4} :other :otherval}}))


(facts "scaled-K"
  (let [K51   (scaled-K 5 1)
        vK51  (vertices K51)]
    (facts "vertices of K51 are correct"
      (count vK51) => 5
      vK51 => #{0 1 2 3 4})
    (tabular
     (fact "edge weights of K51 are correct"
       (edgewt K51 ?v1 ?v2) => 1)
     ?v1 ?v2
     0   1
     1   2
     2   1
     4   3)
    (tabular
     (fact "in K51 there are no self edges"
       (edgewt K51 ?v ?v) => 0)
     ?v
     0
     1
     2
     3
     4)))
