# Clustering Library

ALPHA WORK: this is work in progress, everything is likely to change.

![Travis build status](https://api.travis-ci.org/kasterma/clustering.png)

# Clustering basics

A clustering in the context of this library is a method of grouping
elements from a set according to similarity.  As the notion of
similarity to use depends heavily on the application this is an easy
to use parameter in this library.  The input to the library consists
of an implementation of similarity/ISimilarity

    (defprotocol ISimilarity
      (get-sim [_ pt1 pt2])
      (get-sim-pt-set [_ pt set])
      (get-sim-set-set [_ set1 set2])
      (points [_]))

Using the function max-max-similarity you can get such an
implementation from the set you are interested in with a function
giving pointwise similarity.

The standard example of use can be seen in the function set-of-numbers
which gives the similarity induced from distance on a set of numbers.

# Clusterings available

## Hierarchical clustering

    (cluster {:method :hierarchical
              :similarity (similarity/set-of-numbers #{1 2 4 5})
              :loop-function (create-loop-function)})
    =>
    trace of the clustering.

Note that through use of the loop-function you can indicate what
needs to be saved, and when the clustering can stop.

Here is an example application from the tests using the standard
loop-function created by create-loop-function:

    (cluster {:method :hierarchical
              :similarity (similarity/set-of-numbers #{1 2 4 7})
              :loop-function (create-loop-function)})
    =>
    {1 #{#{1} #{2} #{4} #{7}}, 2 #{#{1 2} #{4} #{7}}, 3 #{#{7} #{1 2 4}}, 4 #{#{1 2 4 7}}}


## k-medoid clustering

    (cluster {:method :k-medoid :similarity similarity :medoid-number k :iteration-no n})
    =>
    k-medoids as obtained after n iterations of the k-medoid algorithm.

Here is an example application from the tests:

    (cluster {:method :k-medoid
              :similarity (similarity/set-of-numbers #{1 2 3 7 8 9 12 13 14})
              :medoid-number 3
              :iteration-no 7})
    => #{2 8 13}

## Fast clustering

This is not the official name for this clustering; I however don't
know an official name, and this is how it came to be know at AdGoji
while I was working with it.  This clustering requires a lot more
structure on the problem to be applicable, at this point it requires
an implementation of PModularityClusterable as found in
cluster.measure

    (cluster {:method :fast :mclusterable mclust})
    =>
    cluster history

# Credits

Some of the code is based on work done for
[AdGoji](http://www.adgoji.com).  Thanks to them for giving me time to
work on this.