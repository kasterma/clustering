(ns cluster.measure
  "Implementations of different measures for clusterings.

   Also includes related helper functions."
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.matrix :as matrix]
            [cluster.graphexamples :as graph]
            [clojure.core.reducers :as r])
  (:import [cluster.graphexamples PWeighedGraph
            weighed-graph WeighedGraphMAT WeighedGraphMATFlex]))

;; # Quality measurements for clusterings.
;;
;; There are many different ways to decide the quality of different
;; clusterings.  Here we define functions for computing the values of
;; some of these measures.
;;
;; # General information
;;
;; ## Clustering-off? predicate
;;
;; A clustering is a partition of a set:
;;
;; 1. all parts are non empty,
;; 2. pairwise disjiont,
;; 3. union is the set is it a partition of.

(defn all-non-empty?
  "predicate returning if all sets in set-o-sets are not empty"
  [set-o-sets]
  (cond
   (zero? (count set-o-sets))
   true

   (= 1 (count set-o-sets))
   (not (empty? (first set-o-sets)))

   :default
   (not (reduce (fn [x y] (or x y)) (map empty? set-o-sets)))))

(defn pairwise-disjoint?
  "predicate returning if the collection set-o-sets is pairwise disjoint"
  [set-o-sets]
  (if (== (count set-o-sets) 1)
    true
    (->> (combinat/combinations set-o-sets 2)
         (map #(set/intersection (first %) (second %)))
         (map empty?)
         (reduce (fn [x y] (and x y))))))

(defn partition?
  "predicate returning if set-o-sets is a disjoint collection of non-empty sets"
  [set-o-sets]
  (and (pairwise-disjoint? set-o-sets)
       (all-non-empty? set-o-sets)))

(defn clustering-of?
  "predicate returning if clustering is a clustering of set"
  [set clustering]
  (and (= set (apply set/union clustering))
       (partition? clustering)))

;; ## Helper functions for clusterings
;;
;; Here we define some functions for use in defining the different measures.

(defn cluster-of
  "return the cluster the element belongs to"
  [clustering element]
  (let [list-clusters (filter #(contains? % element) clustering)]
    ;; list-clusters contains all the parts of the clustering with element in it
    (assert (= 1 (count list-clusters)))
    (first list-clusters)))

(defn same-cluster?
  "predicate returning if elt1 and elt2 are in the same cluster"
  [clustering elt1 elt2]
  (contains? (cluster-of clustering elt1) elt2))

;; ## Helper functions for weighed graphs
;;
;; A weighed graph is determined by a function edge-weights and a set
;; of vertices.

(defn sum-of-weights-into
  "compute the sum of the weights of edges from the given vertex to
  the given cluster"
  [wgraph vertex cluster]
  (apply + (map #(graph/edgewt wgraph vertex %) cluster)))

(defn sum-of-weights
  "compute the sum of the weights of edges leaving the given vertex"
  [wgraph vertex]
  (sum-of-weights-into wgraph vertex (graph/vertices wgraph)))

(defn sum-of-weights-outof
  "compute the sum of the weights of edges from the given clusterin to
  the given vertex"
  [wgraph vertex cluster]
  (apply + (map #(graph/edgewt wgraph % vertex) cluster)))

(defn sum-of-all-weights
  "compute the sum of all weights in the graph"
  [wgraph]
  (try
    (graph/sumofallweights wgraph)
    (catch Exception e
      (do
        (timbre/info
         :sum-of-all-weights
         "called when no efficient version is present")
        (->> (combinat/selections (graph/vertices wgraph) 2)
             (map #(graph/edgewt wgraph (first %) (second %)))
             (apply +))))))

;; ## Modularity
;;
;; This measure is described and used in *Fast unfolding of
;; communities in large networks* by Blondel, Guillaume, Lambiotte,
;; Lefebvre.

(defn modularity-scalar [wgraph]
  (/ 1 (sum-of-all-weights wgraph)))

(defn modularity-term
  ([wgraph clustering elt1 elt2]
     (modularity-term wgraph clustering (modularity-scalar wgraph) elt1 elt2))

  ([wgraph clustering scale elt1 elt2]
     (if (same-cluster? clustering elt1 elt2)
       (- (graph/edgewt wgraph elt1 elt2)
          (* scale (sum-of-weights wgraph elt1) (sum-of-weights wgraph elt2)))
       0)))

(defn modularity
  "compute the modularity of the given clustering"
  [wgraph clustering]
  {:post [(<= % 1)
          (>= % -1)]}
  (let [scale     (modularity-scalar wgraph)]
    (->> (combinat/selections (graph/vertices wgraph) 2)
         (map (fn [[elt1 elt2]] (modularity-term wgraph clustering scale elt1 elt2)))
         (apply +)
         (* scale))))

;; The algorithm described in the paper consists of passes where on
;; each pass every element is moved to a cluster into which it fits
;; best.  The change in modularity obtained by moving an element
;; to a cluster is computed by the function delta-modularity.

;; This function sigma-in is used most often for singleton clusters,
;; but it turns out that first testing if cluster is a set and then,
;; if it is not, using a direct weight lookup is a little slower.

(defn sigma-in
  "compute the total weight of edges between elements of the cluster"
  [wgraph cluster]
  (->> (combinat/cartesian-product cluster cluster)
       (map (partial into []))
       (map (partial graph/edgewt-pair wgraph))
       (remove nil?)
       (apply +)))

(defn get-all-edges-plain
  "get all possible edges between vertices passed in"
  [vertices]
  (combinat/cartesian-product vertices vertices))

(def get-all-edges (memoize get-all-edges-plain))

(defn sigma-tot
  "compute the total weight of edges incident to elements of the cluster
  Note: in the paper *incident* is determined by the starting node"
  [wgraph cluster]
  (let [vertices    (graph/vertices wgraph)]
    (->> (get-all-edges vertices)
         (filter (fn [x] (cluster (first x))))
         (map (partial into []))
         (map (partial graph/edgewt-pair wgraph))
         (remove nil?)
         (apply +))))

(defn sigma-tot-graph-nbdform
  [{:keys [nbdmap]} cluster]
  (apply + (map (comp #(get :total-out % 0) nbdmap) cluster)))

(def sigma-tot-m (memoize sigma-tot))

(defn delta-modularity
  "compute the change in modularity from elt getting moved to cluster"
  [wgraph cluster elt]
  (profiling/profile :trace :delta-mod
                     (let [trace     (fn [a b] b) ; replace by tt/trace to see the computations
                           p         (fn [a b] b) ; replace by profiling/p to get profiling active
                           sig-in    (trace :sigma-in (p :sigma-in (sigma-in wgraph cluster)))
                           k-i-in    (trace :kiin (p :sum-into (sum-of-weights-into wgraph elt cluster)))
                           k-in-i    (trace :kini (p :sum-outof (sum-of-weights-outof wgraph elt cluster)))
                           scalar    (p :mod-scalar (modularity-scalar wgraph))
                           scale     (fn [x] (* scalar x))
                           square    (fn [x] (* x x))
                           sig-tot   (trace :sigtot (p :sigma-tot (sigma-tot wgraph cluster)))
                           k-i       (trace :ki (p :sow (sum-of-weights wgraph elt)))]
                       (p :compute (- (- (scale (+ sig-in k-i-in k-in-i))
                                         (square (scale (+ sig-tot k-i))))
                                      (- (scale sig-in)
                                         (square (scale sig-tot))
                                         (square (scale k-i))))))))

;; ## With neighborhood representations
;;
;; Also note that the formulas have been specialized to joining nodes,
;; they are no longer generally useable for clusters and points.

(defn delta-modularity-nbd
  "compute the change in modularity from the element getting joined with
   other element.

   Note: fails with division by 0 on a graph with no edges."
  [{:keys [vertices nbdmap] :as graph-nbb-rep} elt-c elt-add]
  {:pre [(contains? vertices elt-c)
         (contains? vertices elt-add)]}
  (let [trace               (fn [a b] b)
                            ;tt/trace
        get-in-nbd          (fn [keys] (get-in nbdmap keys 0))
        sig-in              (trace :sigma-in-b (get-in-nbd [elt-c :edges elt-c]))
        k-i-in              (trace :kiin-b (get-in-nbd [elt-add :edges elt-c]))
        k-in-i              (trace :kini-b (get-in-nbd [elt-c :edges elt-add]))
        modularity-scalar   (/ 1 (apply + (remove nil? (reduce into [] (map vals (map :edges (vals nbdmap)))))))
        scale               (fn [x] (* modularity-scalar x))
        square              (fn [x] (* x x))
        sig-tot             (trace :sigtot-b (apply + (vals (get-in-nbd [elt-c :edges]))))
        k-i                 (trace :ki-b (apply + (vals (get-in-nbd [elt-add :edges]))))]
    (- (- (scale (+ sig-in k-i-in k-in-i))
          (square (scale (+ sig-tot k-i))))
       (- (scale sig-in)
          (square (scale sig-tot))
          (square (scale k-i))))))

;; ## With matrix representation

(defn delta-modularity-matrix
  "compute the change in modularity from the element getting joined with
  other element."
  [{:keys [vertices->idx rowsums modularity-scalar nbdmatrix] :as graph-matrix-rep} elt-c elt-add]
  {:pre [(contains? (set (keys vertices->idx)) elt-c)
         (contains? (set (keys vertices->idx)) elt-c)]}
  (let [elt-c-idx       (vertices->idx elt-c)
        elt-add-idx     (vertices->idx elt-add)
        trace           (fn [a b] b) ; choose this for no tracing, next for tracing
                        ;tt/trace
        sig-in          (trace :sigma-in-c (matrix/mget nbdmatrix (trace :elts elt-c-idx) elt-c-idx))
        k-i-in          (trace :kiin-c (matrix/mget nbdmatrix elt-add-idx elt-c-idx))
        k-in-i          (trace :kini-c (matrix/mget nbdmatrix elt-c-idx elt-add-idx))
        scale           (fn [x] (* modularity-scalar x))
        square          (fn [x] (* x x))
        sig-tot         (trace :sigtot-c (rowsums elt-c-idx))
        k-i             (trace :ki-c (rowsums elt-add-idx))]
    (- (- (scale (+ sig-in k-i-in k-in-i))
          (square (scale (+ sig-tot k-i))))
       (- (scale sig-in)
          (square (scale sig-tot))
          (square (scale k-i))))))

(defprotocol PModularityClusterable
  "similarity data with implementations for modularity clustering"

  (deltamodularity [this node1 node2]
    "the delta in modularity for collapsing the two nodes")

  (collapsenodes [this node1 node2]
    "returns the Modularity Clusterable with the two nodes identified")

  (othernodes [this node]
    "returns the set of nodes in the graph other than node")

  (findbest [this node]
    "returns the optimal node and the change in delta it would entail")

  (takestep [this node]
    "returns the new modularity clusterable with the step related to
    node taken"))


;; ### Collect all data and code together for the matrix clustering
;;
;; In this record deltamodularity and collapsenodes are really just
;; wrappers to get the arguments correct and extract the information
;; from the return values.
;;
;; The other methods contain the keys steps for the fast clustering
;; algorithm.

(defn- maxvr
  "max value reducer (for a context where a 0 value means no action
  will be taken)"
  ([] [:invalid 0])
  ([x y]
     (if (> (second x) (second y)) x y)))

(defrecord MatrixClusterable [weighedgraphmatflex
                              rowsums
                              modularityscalar
                              clustermap
                              reducers?]
  PWeighedGraph
  (vertices [this]
    (graph/vertices weighedgraphmatflex))

  (edgewt [this node1 node2]
    (graph/edgewt weighedgraphmatflex node1 node2))

  PModularityClusterable
  (deltamodularity [this node1 node2]
    (let [vert->idx   (:vertex->idx weighedgraphmatflex)]
      (delta-modularity-matrix {:vertices->idx vert->idx
                                :rowsums rowsums
                                :modularity-scalar modularityscalar
                                :nbdmatrix (:matrix weighedgraphmatflex)}
                               node1
                               node2)))

  (collapsenodes [this node1 node2]
    (let [coll-result (graph/collapse-nodes-matrix-representation
                       {:vertices->idx (:vertex->idx weighedgraphmatflex)
                        :vertex-cluster-map (clustermap (apply max (keys clustermap)))
                        :rowsums rowsums
                        :modularity-scalar modularityscalar
                        :nbdmatrix (:matrix weighedgraphmatflex)}
                       node1
                       node2
                       (deltamodularity this node1 node2))]
      (MatrixClusterable. (WeighedGraphMATFlex. (set (keys (:vertices->idx coll-result)))
                                                (:vertices->idx coll-result)
                                                (:nbdmatrix coll-result))
                          (:rowsums coll-result)
                          (:modularity-scalar coll-result)
                          (assoc clustermap
                            (inc (apply max (keys clustermap)))
                            (:vertex-cluster-map coll-result))
                          reducers?)))

  (othernodes [this node]
    (disj (graph/vertices weighedgraphmatflex) node))

  (findbest [this node]
    (let [other-nodes        (othernodes this node)
          deltas             ((if reducers? r/map pmap)
                              (fn [on] [on (deltamodularity this on node)])
                              other-nodes)]
      ;;randomly pick first if multiple with max val
      (if reducers?
        (r/fold maxvr deltas)
        (apply max-key second deltas))))

  (takestep [this node]
    (if (contains? (graph/vertices weighedgraphmatflex) node)
      (let [[best-node delta-mod]  (findbest this node)]
        (if (pos? delta-mod)
          (collapsenodes this node best-node)
          this))
      (do
        (timbre/info :takestek "not a valid vertex")
        this)))) ;; this node was already collapsed with another at an earlier stage

(defn createMatrixClusterable
  "given a WeighedGraphMAT construct a MatrixClusterable from it"
  ([wgm]
     (createMatrixClusterable wgm :use-reducers))

  ([wgm use-reducers?]
     (let [vertices            (:vertices wgm)
           nbdmatrix           (:matrix wgm)
           vertex-cluster-map  (zipmap vertices (mapv (fn [x] #{x}) vertices))
           rowsums             (zipmap (range 0 (matrix/row-count nbdmatrix))
                                        (mapv matrix/esum (matrix/slices nbdmatrix)))
           _                   (timbre/info "about to divide by:" (matrix/esum nbdmatrix))
           modularity-scalar   (/ 1 (matrix/esum nbdmatrix))]
       (MatrixClusterable. wgm
                           rowsums
                           modularity-scalar
                           {0 vertex-cluster-map}
                           (= use-reducers? :use-reducers)))))




;; ## Statistics of Clusters
;;
;; The following is a collection of different statistics for
;; clusterings so as to be able to keep an eye on the running of the
;; algorithm.
;;
;; Note: since this code should also work on statistically generated
;; data we use (>= similarity 1) in stead of (= similarity 1) to check
;; for elements representing the same elements.

(defn cluster-size-statistics
  "compute some statistics for the cluster sizes"
  [clusters]
  (let [sizes        (sort (map count clusters))
        num          (count sizes)
        min-size     (apply min sizes)
        max-size     (apply max sizes)
        mean-size    (double (/ (apply + sizes) (count sizes)))
        quart1       (nth sizes (max 0 (dec (Math/floor (/ num 4)))))
        quart2       (nth sizes (max 0 (dec (Math/floor (/ num 2)))))
        quart3       (nth sizes (max 0 (dec (* 3 (Math/floor (/ num 4))))))]
    {:number num
     :min min-size
     :max max-size
     :mean mean-size
     :spread (- max-size min-size)
     :quartiles [quart1, quart2, quart3]}))

(defn min-similarity
  "Compute the minimum similarity in the cluster

   Note: returns :NaN for a singleton cluster.

   The optional argument dropones can be set to true to drop similarity
   values >= 1 from consideration.  For many similarity measures a
   similarity equal to 1 indicates the objects are identical; in those
   situations you want to treat the objects as the same so e.g. if a
   cluster only has similarity scores equal or greater than 1 return
   Double/NaN"
  ([cluster similarity-map]
     (min-similarity cluster similarity-map false)
                                        ; false is anything different than :dropones
     )

  ([cluster similarity-map dropones]
     {:pre [(seq cluster)]
      :post [(or (= :NaN %) (pos? %))]}
     (let [similarities
           (map similarity-map (combinat/combinations cluster 2))]
       (if (some keyword? similarities)
         :NaN
         (let [similarities-filtered
               (remove #(and (= dropones :dropones) (>= % 1)) similarities)]   ; TODO optimize

           (if (pos? (count similarities-filtered))
             (apply min similarities-filtered)
             :NaN))))))

(defn max-inter-cluster-similarity
  "Compute the largest similarity that occurs between the two clusters.

   Note: for most similarities a value of 1 indicates the clusters contain
   representations of the same element."
  [cluster-a cluster-b similarity-map]
  {:pre [(seq cluster-a)
         (seq cluster-b)
         (empty? (set/intersection (set cluster-a) (set cluster-b)))]
   :post [(>= % 0)]}
  (->> (combinat/cartesian-product cluster-a cluster-b)
       (map #(get similarity-map % 0))
       (apply max)))

(defn min-cluster-similarity
  "Compute the minimum similarity that occurs inside a single cluster.

   Note: returns :NaN if all clusters are singletons.

   The optional argument dropones set to true is passed to
   min-similarity ensuring that clusters with all elements with
   similarity 1 are treated as singleton clusters."
  ([clusters similarity-map]
     (min-cluster-similarity clusters similarity-map false)
                                             ; false is just anything not equal to :dropones
     )

  ([clusters similarity-map dropones]
      {:pre [(pos? (count clusters))]
       :post [(or (= :NaN %) (pos? %))]}
      (let [similarities
            (filter #(not= :NaN %)
                    (map #(min-similarity % similarity-map dropones)
                         clusters))]

        (if (pos? (count similarities))
          (apply min similarities)
          :NaN))))

;; ### For similarities that give rise to a metric
;;
;; These similarity measurements are intended to be used with
;; similarity measurements such that
;;
;;    (1 - similarity)
;;
;; is a metric.
;;
;; This code also works for situations where there are multiple
;; representations of the same point, we pass :dropones to the
;; above similarity computations.

(defn cluster-diameter
  "Compute the diameter of a cluster"
  [cluster similarity-map]
  (- 1 (min-similarity cluster similarity-map :dropones)))

(defn cluster-distance
  "return the inter cluster distance"
  [cluster-a cluster-b similarity-map]
  {:pre [(seq cluster-a)
         (seq cluster-b)
         (empty? (set/intersection (set cluster-a) (set cluster-b)))]
   :post [(not (= % 0))]}
  (- 1 (max-inter-cluster-similarity cluster-a cluster-b similarity-map)))

;; #### Davis Bouldin index
;;
;; Compute the Davis Bouldin index.  There are some choices to be
;; made, we choose to represent cluster compactness by cluster
;; diameter and inter cluster spread by the minimum pointwise
;; difference between the clusters (these two measures are exactly the
;; ones just implemented).

(defn Rij-DD
  "subcomputation for Davis Bouldin"
  [cluster-a cluster-b similarity-map]
  (let [diam-a      (cluster-diameter cluster-a similarity-map)
        diam-b      (cluster-diameter cluster-b similarity-map)
        inter-a-b   (cluster-distance cluster-a cluster-b similarity-map)]
    (double (/ (+ diam-a diam-b) inter-a-b))))

(defn Ri-DD
  "subcomputation for Davis Bouldin"
  [cluster-a clusters similarity-map]
  (let [other-clusters   [];(remove-clusters clusters [cluster-a])
        ]
    (apply max (map #(Rij-DD cluster-a % similarity-map) other-clusters))))

(defn davis-bouldin-index
  "compute the davis bouldin index for the given clustering"
  [clusters similarity-map]
  (apply min (map #(Ri-DD % clusters similarity-map) clusters)))


;; #### Dunn index #########################################################################
;;
;; Compute the Dunn index.  Here we use the same choices as we made
;; for the Davis Bouldin index.

(defn min-inter-cluster-distances
  "compute the minimum cluster-distance between the num-th cluster and all later ones"
  [clusters num similarity-map]
  {:pre [(>= num 0)
         (< num (count clusters))]}
  (let [clust             (nth clusters num)
        later-clusters    (drop (inc num) clusters)]
    (apply min (map #(cluster-distance clust % similarity-map)))))

(defn dunn-index
  "compute the dunn index for the given clustering"
  [clusters similarity-map]
  (let [max-diam         (apply max (map #(cluster-diameter % similarity-map) clusters))
        min-inter-dist   (apply min (map #(min-inter-cluster-distances clusters % similarity-map)
                                         (range 0 (count clusters))))]
    (double (/ min-inter-dist max-diam))))
