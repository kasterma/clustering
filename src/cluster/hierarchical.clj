(ns cluster.hierarchical
  "Implementation of hierarchical clustering.

   (cluster {:method :hierarchical
             :similarity (similarity/set-of-numbers #{1 2 4 5})
             :loop-function (create-loop-function)})
   =>
   trace of the clustering.

   Note that through use of the loop-function you can indicate what
   needs to be saved, and when the clustering can stop."
  (:use [cluster.core])
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [cluster.measure :as meas]
            [cluster.similarity :as similarity]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [schema.core :as s]
            [schema.macros :as sm]))


;; ## Hierarchical Clustering
;;
;; In this clustering we repeatedly look at which two clusters
;; currently are most similar and join these into one cluster.  We
;; start with all elements in a cluster by themselves.

(defn initial-clustering
  "get the initial clustering from the similarity"
  [similarity]
  (->> (similarity/points similarity)
       (map (fn [x] #{x}))
       set))

(sm/defn find-closest-clusters :- {:cluster1 #{s/Any}
                                   :cluster2 #{s/Any}
                                   :similarity s/Number}
  "find the most similar clusters."
  [similarity :- (s/protocol similarity/ISimilarity)
   clusters :- (s/both #{#{s/Any}}
                       (s/pred #(<= 2 (count %))))]

  (let [closest (->> (combinat/combinations clusters 2)
                     (map (fn [[x y]]
                            [(similarity/get-sim-set-set similarity x y) x y]))
                     (apply max-key first))]
    {:cluster1 (second closest)
     :cluster2 (nth closest 2)
     :similarity (first closest)}))

(sm/defn remove-clusters :- #{#{s/Any}}
  "return the list of all clusters that are not in cluster-remove"
  [clusters :- #{#{s/Any}}
   cluster-remove-set :- #{#{s/Any}}]
  (set (remove #(cluster-remove-set %) clusters)))

(defn create-loop-function
  "create a loop function that constructs and returns the trace of the
   clustering"
  []
  (let [trace (atom {})]
    (fn
      ([] @trace)
      ([clusters iteration]
         (swap! trace assoc iteration clusters)
         false))))

(defmethod cluster :hierarchical [{:keys [similarity loop-function] :as inval}]
  "Perform the hierarchical clustering, loop-function will be called
   on the clustering at the start of every loop.  As soon as
   loop-function returns true, or there is only one cluster, the
   clustering stops.  The loop-function called with no arguments gives
   the return value of the clustering."
  (loop [clusters (initial-clustering similarity)
         iteration 1]
    (let [loop-value (loop-function clusters iteration)]
      (if (or loop-value (== (count clusters) 1))
        (loop-function)
        (let [closest-clusters     (find-closest-clusters similarity clusters)
              untouched-clusters   (remove-clusters clusters
                                                    #{(:cluster1 closest-clusters)
                                                      (:cluster2 closest-clusters)})
              new-cluster          (set/union (:cluster1 closest-clusters) (:cluster2 closest-clusters))
              new-clusters         (conj untouched-clusters new-cluster)]
          (recur new-clusters (inc iteration)))))))

