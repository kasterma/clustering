(ns cluster.graphexamples
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.matrix :as matrix]
            [clojure.core.matrix.operators :as matrixo])
  (:gen-class))

(matrix/set-current-implementation :vectorz)

;; # Generate different graphs to run other algorithms on
;;
;; A weighed graph is a set of vertices and weights assigned to pairs
;; of vertices. A pair getting a weights zero assigned is equivalent
;; to there not being an edge.

;; ## Weighed graph protocol
;;
;; This protocol represents a weighed graph in its simplest form, as a
;; set of vertices and a method for getting weights of edges.

(defprotocol PWeighedGraph
  "interface for a weighed graph"
  (vertices [this]
    "return the set of vertices")

  (edgewt [this vertex1 vertex2]
    "return the weight assigned to the edge from vertex1 to vertex2"))

(defn edgewt-pair
  [wgraph [elt1 elt2]]
  (edgewt wgraph elt1 elt2))

(defprotocol PSumOfAllWeights
  "interface for a graph that has an efficient method of summing all weigths"
  (sumofallweights [this]))

;; ### Representation using map
;;
;; In this prepresentation the weighed graph uses a map to store the
;; edge weights.  This map takes as input pairs [vertex1 vertex2] and
;; as output gives a double that is the weight of the edge.
;;
;; Example map:
;;
;;     {[1 2] 3, [4 5] 6}
;;
;; In this map the edge from 1 to 2 has weight 3, and the edge from 4
;; to 5 has weight 6.

(defrecord weighed-graph [vertices edgeweightsmap]
  PWeighedGraph
  (vertices [this] vertices)

  (edgewt [this vertex1 vertex2]
    (edgeweightsmap [vertex1 vertex2] 0)))

(defn weighed-graph?
  "predicate for being a weighed graph with proper attributes.

   Checks the vertices and map are of the right type, and all keys in
   the map are pairs of vertices.

   Note: this predicate can become false by certain operations on the
   graph."

  [wgraph]
  (let [vert (vertices wgraph)
        ewm  (:edgeweightsmap wgraph)]
    (if (or (nil? vert) (nil? ewm) (not (set? vert)) (not (map? ewm)))
      false
      (->> (keys ewm)
           (reduce into #{})
           (every? (partial contains? vert))))))

(defn clean-weighed-graph
  "On input a weighed graph record returns a weighed graph that will
   pass the predicate weiged-graph?; to do this it removes items from
   the map to only have vertices in the pairs."

  [wgraph]
  (if (weighed-graph? wgraph)
    wgraph
    (let [vert-orig    (vertices wgraph)
          vert         (if (set? vert-orig)
                         vert-orig
                         #{})
          ewm-orig     (:edgeweightsmap wgraph)
          ewm          (if (map? ewm-orig)
                         (select-keys ewm-orig (combinat/selections vert 2))
                         {})]
      (weighed-graph. vert ewm))))

;; ### Neighbor representation
;;
;; In this representation the neighbors and edge information is stored
;; in a map. This map takes a vertex as its input, and outputs a map
;; that on input :edges gives a map that in turn on a vertex outputs
;; the weight between the two vertices.
;;
;; Example map:
;;
;;    {1 {:edges {2 1, 1 3}}
;;     2 {:edges {1 5, 4 6}}}
;;
;; In this example there are edges [1 2] with weight 1, [1 1] with
;; weight 3, [1 2] with weight 5, and [2 4] with weight 6.
;;
;; The reason for the :edges keyword is to leave room for other data
;; associated to the vertex.

(defrecord WeighedGraphNBD [vertices nbdmap]
  PWeighedGraph
  (vertices [this] vertices)

  (edgewt [this vertex1 vertex2]
    ((get-in nbdmap [vertex1 :edges] (constantly 0)) vertex2 0)))

;; ### Matrix representation
;;
;; In this representation the edge information is stored in a
;; core.matrix object.  In this map the weight of an edge from vertex1
;; to vertex2 is stored in location [vertex1, vertex2].

(defrecord WeighedGraphMAT [vertices matrix]
  PWeighedGraph
  (vertices [this] vertices)

  (edgewt [this vertex1 vertex2]
    (matrix/mget matrix vertex1 vertex2))

  PSumOfAllWeights
  (sumofallweights [this]
    (matrix/esum matrix)))

;; ### More flexible matrix representation
;;
;; In this matrix representation the names of the vertices are not
;; required to be integers (that can be used as indices to a matrix).
;; So next to the matrix there is a mapping translating vertex names
;; to integers.

(defrecord WeighedGraphMATFlex [vertices vertex->idx matrix]
  PWeighedGraph
  (vertices [this] vertices)

  (edgewt [this vertex1 vertex2]
    (matrix/mget matrix (vertex->idx vertex1) (vertex->idx vertex2)))

  PSumOfAllWeights
  (sumofallweights [this]
    (matrix/esum matrix)))

(defn WeighedGraphMATFlex?
  "predicate to check consistency of the object"

  [wgraph]

  (let [vert      (vertices wgraph)
        v->idx    (:vertex->idx wgraph)
        mat       (:matrix wgraph)]
    (if (or (nil? vert) (nil? v->idx) (nil? mat)
            (not (set? vert)) (not (map? v->idx)) (not (matrix/matrix? mat))
            (not (every? (partial contains? vert) (keys v->idx))))
      (do
        (timbre/info :WeighedGraphMATHFlex? "type error")
        false)
      (let [rc      (matrix/row-count mat)
            cc      (matrix/column-count mat)
            unused  (set/difference (set (range 0 cc)) (set (vals v->idx)))]
        (if (or (not (= rc cc)) (> (apply max (vals v->idx)) rc))
          (do
            (timbre/info :WeighedGraphMATHFlex? "range errro")
            false)
          (if (and (every? (fn [[f s]] (zero? (matrix/mget mat f s)))
                           (combinat/cartesian-product unused (range 0 cc)))
                   (every? (fn [[f s]] (zero? (matrix/mget mat f s)))
                           (combinat/cartesian-product (range 0 cc) unused)))
            true
            (do
              (timbre/info :WeighedGraphMATHFlex? "nonzero error")
              false)))))))

(defn clean-WeighedGraphMATFlex
  "change a weighed graph so that it passes the predicate.

   Actually only set matrix values to zero that should be zero.
   Since updates to the matrix are in place, this function changes
   the passed object."

  [wgraph]
  (let [vert      (vertices wgraph)
        v->idx    (:vertex->idx wgraph)
        mat       (:matrix wgraph)
        rc      (matrix/row-count mat)
        cc      (matrix/column-count mat)
        unused  (set/difference (set (range 0 cc)) (set (vals v->idx)))]
    (doseq [p  (combinat/cartesian-product unused (range 0 cc))]
      (fn [[f s]] (zero? (matrix/mget mat f s))))
    (doseq [p  (combinat/cartesian-product (range 0 cc) unused)]
      (fn [[f s]] (zero? (matrix/mget mat f s))))
    (WeighedGraphMATFlex. vert v->idx mat)))

;; ## Conversions between the different graph types
;;
;; This representation was suggested by discussion with Gijs and
;; Jeroen, and in an example looks like:
;;
;; (def graph
;; { :total-weight 10000
;;   :vertset #{:a :c :d}
;;   :vertices {
;;       :a { :total-weight 100 :edges { :a 1 :b 2 } :orig-vertices #{:a :b}}
;;       :b { :total-weight 100 :edges { :a 1 :c 2 } }
;;       :c { :total-weight 100 :edges { :a 1 :b 6 :c 2 } }
;;       :d { :total-weight 100 :edges { :a 1 :c 2 } }
;;   ]}
;; )


;; ### Conversions to WeighedGraphMATFlex
;;
;; This method is intended to implement the different conversions to
;; WeighedGraphMATFlex representation.

(defmulti getMATFlex class)

(defmethod getMATFlex weighed-graph [wgraph]
  (let [vertices   (vertices wgraph)
        v-count    (count vertices)
        vert->id   (zipmap vertices (range 0 v-count))
        mat        (matrix/new-matrix v-count v-count)]
    (doseq [[f s] (combinat/selections vertices 2)]
      (matrix/mset! mat (vert->id f) (vert->id s) (edgewt wgraph f s)))
    (WeighedGraphMATFlex. vertices vert->id mat)))

;; ### Conversions to WeighedGraphNBD
;;
;; This method is intended to implement the different conversion to
;; WeighedGraphNBD representation.  This representation used to have a
;; lot of additional cached values, these have been removed for
;; simplicity and since the fasted representation is now the matrix
;; representation.  This is only keps around otherwise unchanged for
;; possible future use.

(defmulti getNBDrep class)

(defn edges-out->nbd-rep
  "given a set of edges and the edge-weights map compute the
  neighbor-weight representation.

  Note: the assumption (checked) is that all the first members are
  equal."
  [edges edge-weights]
  {:pre [(>= 1 (count (set (map first edges))))]}

  (reduce (fn [m p] (assoc m (second p) (edge-weights p 0))) {} edges))

(defn- add-vertex-edges
  "function to be used in creating the nbdmap of nbd representation.

  This function computes the value of nbdmap for the given vertex and assocs
  it onto the partmap passed."
  [edge-weights edges-grouped-out-of edges-grouped-into partmap vertex]

  (let [edges-out            (edges-grouped-out-of vertex [])
        edges-in             (edges-grouped-into vertex [])
        weighed-neighbors    (edges-out->nbd-rep edges-out edge-weights)]
    (assoc partmap vertex {:edges weighed-neighbors})))

;; Convert the vertices and ege-weights representation to vertices
;; with neighbors representation.

(defmethod getNBDrep weighed-graph
  [{:keys [vertices edgeweightsmap] :as wgraph}]

  (let [edges                    (keys edgeweightsmap)
        edges-grouped-out-of     (group-by first edges)
        edges-grouped-into       (group-by second edges)
        create-edge-map          (partial add-vertex-edges
                                          edgeweightsmap
                                          edges-grouped-out-of
                                          edges-grouped-into)
        nbdmap                   (reduce create-edge-map {} vertices)]
    (WeighedGraphNBD. vertices
                      nbdmap)))







;; ## Fast Clustering Related Operations
;;
;; The key graph operation that needs to be implemented is the
;; collapse of two nodes to a single node.  The general operation is
;; the collapse of a set of nodes, but in the actual clustering we
;; only need the case of two nodes.

(defn- combine
  "given the weighed nbd and two nodes return the map that represents
  the values after collapsing the two nodes.

  Note: the second node is not removed, just has its value set to zero."
  [nbds node1 node2]

  (assoc (update-in nbds
                    [node1]
                    (fn [node1-val] (+ (get nbds node2 0) (or node1-val 0))))
         node2
            0))

(def combine-wrapper (fn [node1 node2 [vertex {:keys [edges] :as m}]]
                       {vertex (merge m
                                      {:edges (combine edges node1 node2)})}))

(defn collapse-nodes-nbd-representation
  "collapse two nodes in nbd representation.

  node1 becomes the new collapsed node
  node2 has weights set to 0, but otherwise stays around
  (at some point could introduce a clean function to remove
  all stuff left over this way)."
  [{:keys [vertices nbdmap]} node1 node2]

;; node1 total out = + total out both nodes
;; node1 total in = + total in both nodes
;; node1-> node-other = + value from node1 and node2 to node-other
;; node-other -> node1 = + value to node1 and node2

  (let [get-in-nbd     (partial get-in nbdmap)
        af             (partial combine-wrapper node1 node2)
        edges-out      (combine (merge-with +
                                            (get-in-nbd [node1 :edges])
                                            (get-in-nbd [node2 :edges]))
                                node1
                                node2)
        other-edges    (apply merge (map af nbdmap))
        n-nbdmap       (merge other-edges  {node1 {:edges edges-out}})]
    {:vertices (disj vertices node2)
     :nbdmap n-nbdmap}))

(defn update-matrix-value [matrix xu yu xf yf]
  "replace the value at (xy, yu) by adding to it the value at (xf, yf)"
  (let [v1 (matrix/mget matrix xf yf)
        v2 (matrix/mget matrix xu yu)]
    (matrix/mset! matrix xu yu (+ v1 v2))))

(defn update-matrix
  [matrix node1 node2]
  (let [msize   (matrix/column-count matrix)] ; matrix is square so is also row-count
    ;; add the values from row node2 to row node1
    (doseq [y (range 0 msize)] (update-matrix-value matrix node1 y node2 y))
    ;; add the values from column node2 to column node2
    (doseq [x (range 0 msize)] (update-matrix-value matrix x node1 x node2))))


(defn collapse-nodes-matrix-representation
  "collapse two nodes in nbd representation.

  node1 becomes the new collapsed node
  node2 has weights set to 0

  Tested mostly in measures_test.clj"

  [{:keys [vertices->idx vertex-cluster-map rowsums modularity-scalar nbdmatrix]}
   node1 node2 deltam]

  (let [node1-idx               (vertices->idx node1)
        node2-idx               (vertices->idx node2)
        new-vertices->idx       (dissoc vertices->idx node2)
        new-vertex-cluster-map  (dissoc (update-in vertex-cluster-map [node1]
                                              #(set/union % (vertex-cluster-map node2)))
                                      node2)
        new-rowsums             (update-in rowsums [node1-idx] #(+ % (rowsums node2-idx)))
        new-modularity-scalar   (/ 1 (+ (/ 1 modularity-scalar) deltam))]
    (update-matrix nbdmatrix node1-idx node2-idx) ; updates matrix in place
    {:vertices->idx new-vertices->idx
     :vertex-cluster-map new-vertex-cluster-map
     :rowsums new-rowsums
     :modularity-scalar new-modularity-scalar
     :nbdmatrix nbdmatrix ; was updated in place
     }))


(defn matrix-graph->clustering
  "extract the current clustering from the matrix representation"
  [{:keys [vertex-cluster-map]}]
  (vals vertex-cluster-map))

;; # Graph Examples
;;
;; ## Weighed K
;;
;; The full *simple* graph with given fixed edge weight.

(defn scaled-K
  "return the full graph with fixed identical edge weigths"
  [size edge-wt]

  (let [vertices       (set (range 0 size))
        edge-weights   (reduce (fn [m pair] (assoc m pair edge-wt (vec (reverse pair)) edge-wt))
                               {}
                               (map vec (combinat/combinations vertices 2)))]
    (weighed-graph. vertices edge-weights)))

;; # Bipartite full edges between
;;
;; The full bipartite graph with given fixed edge weight.

(defn full-bipartite
  "create the full bipartite graph with given identical edge weigths"
  [size-A size-B edge-wt]

  (let [vertices-A   (set (map (partial * 2) (range 0 size-A)))
        vertices-B   (set (map (fn [x] (inc (* 2 x))) (range 0 size-B)))
        vertices     (set/union vertices-A vertices-B)
        edge-weights (reduce (fn [m pair] (assoc (assoc m pair edge-wt) (reverse pair) edge-wt)) {}
                             (combinat/cartesian-product vertices-A vertices-B))]
    (weighed-graph. vertices edge-weights)))

(defn relabel-graph
  "relabel the vertices of the graph using the function label"
  [wgraph label]

  (let [vertices        (set (map label (:vertices wgraph)))
        we              (:edgeweightsmap wgraph)
        weighed-edges   (reduce (fn [m pair] (assoc m (mapv label pair) (we pair)))
                                {}
                                (keys we))]
    (weighed-graph. vertices weighed-edges)))

(defn disjoint-union
  "compute a disjoint union of the two weighed graphs"
  [wgraph1 wgraph2]

  (let [ver1         (:vertices wgraph1)
        ver2         (:vertices wgraph2)
        disjoint     (empty? (set/intersection ver1 ver2))]
    (if disjoint
      (let [vertices          (set/union ver1 ver2)
            weighed-edges     (merge (:edgeweightsmap wgraph1) (:edgeweightsmap wgraph2))]
        (weighed-graph. vertices weighed-edges))

      (let [label-A           (partial str "A-")
            label-B           (partial str "B-")
            wgraphA           (relabel-graph wgraph1 label-A)
            wgraphB           (relabel-graph wgraph2 label-B)]
        (disjoint-union wgraphA wgraphB)))))

(defn find-fresh-node-name
  "find a name that is not the name of a node in weighed-graph starting from the base"
  [weighed-graph base]
  (let [vertices    (:vertices weighed-graph)]
    (loop [attempt   base]
      (if (contains? (set vertices) (keyword attempt))
        (recur (str attempt "'"))
        (keyword attempt)))))

(defn collapse-nodes
  "compute the weighed graph where the two input nodes have been contracted to one node"

  ([w-graph node1 node2]
     (collapse-nodes w-graph node1 node2 :discard-edges))

  ([w-graph node1 node2 keep-edges?]
     {:pre [(contains? (vertices w-graph) node1)
            (contains? (vertices w-graph) node2)
            (or (= keep-edges? :discard-edges) (= keep-edges? :keep-edges))]}
     (let [fresh-node          (find-fresh-node-name w-graph (str node1 node2))
           old-nodes           #{node1 node2}
           fixed-nodes         (set/difference (:vertices w-graph) old-nodes)
           new-nodes           (into fixed-nodes [fresh-node])
           edge-weights        (:edgeweightsmap w-graph)
           edges               (keys edge-weights)]

       (if-not (empty? edges)

         (let [unaffected-edges-map    (if (= keep-edges? :keep-edges)
                                         edge-weights
                                         (->> (keys edge-weights)
                                              (remove (fn [[s t]] (or (#{node1 node2} s)
                                                                      (#{node1 node2} t))))
                                              (reduce (fn [m edge] (assoc m edge (edge-weights edge)))
                                                      {})))
               fresh-node-self-map     {[fresh-node fresh-node]
                                        (->> (combinat/cartesian-product old-nodes old-nodes)
                                             (map #(edge-weights % 0))
                                             (apply +))}
               to-fresh-node           (->> fixed-nodes
                                            (map (fn [node] [node (+ (edge-weights [node node1] 0)
                                                                     (edge-weights [node node2] 0))]))
                                            (reduce (fn [m [node val]]
                                                      (assoc m [node fresh-node] val))
                                                    {}))
               from-fresh-node         (->> fixed-nodes
                                            (map (fn [node] [node (+ (edge-weights [node1 node] 0)
                                                                     (edge-weights [node2 node] 0))]))
                                            (reduce (fn [m [node val]]
                                                      (assoc m [fresh-node node] val))
                                                    {}))]
           (weighed-graph. new-nodes (merge unaffected-edges-map
                                           fresh-node-self-map
                                           to-fresh-node
                                           from-fresh-node)))

         ;; the graph has no edges, just return a graph with the nodeset changed
         (weighed-graph. new-nodes {})))))

;; # Circular example graph

(defn circular-first-ith-clique
  "return the first element of the ith-clique"
  [clique-size i] (* clique-size i))

(defn circular-last-ith-clique
  "return the last element of the ith-clique"
   [clique-size i] (dec (* clique-size (inc i))))

(defn circular-ith-clique
  "return a vector representing the elements of the ith clique"
  [clique-size i]

  (range (circular-first-ith-clique clique-size i)
         (inc (circular-last-ith-clique clique-size i))))

(defn circular
  "create the circular cluster graph"
  [clique-no clique-size]

  (let [vertices            (set (range 0 (* clique-no clique-size)))
        first-ith           (partial circular-first-ith-clique clique-size)
        last-ith            (partial circular-last-ith-clique clique-size)
        ith-clique          (partial circular-ith-clique clique-size)
        ith-clique-edges    (comp (partial map vec)
                                  #(combinat/cartesian-product % %)
                                  ith-clique)
        clique-edges        (mapcat ith-clique-edges (range 0 clique-no))
        between-edges       (map
                             (fn [i] [(last-ith i) (first-ith (inc i))])
                             (range 0 (dec clique-no)))
        finish-edge         [(last-ith (dec clique-no)) 0]
        edges               (conj (concat clique-edges between-edges) finish-edge)
        edge-weights        (reduce (fn [m edge] (assoc m edge 1)) {} edges)]
    (weighed-graph. vertices edge-weights)))
