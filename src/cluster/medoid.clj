(ns cluster.medoid
  "Implementation of k-medoid clustering.

   (cluster {:method :k-medoid :similarity similarity :medoid-number k :iteration-no n})
   =>
   k-medoids as obtained after n iterations of the k-medoid algorithm."
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [taoensso.timbre
             :as timbre
             :refer (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :refer (p profile)]
            [schema.core :as s]
            [schema.macros :as sm]
            [cluster.similarity :as similarity])
  (:use [cluster.core]))

;; ## Medoid Clustering

(defn pick-medoids-h
  "helper function for picking the medoids (picked is the accumulator variable)"
  [points number-to-pick picked]
  (if (or (zero? number-to-pick) (empty? points))
    picked
    (let [new-point    (rand-nth points)
          rem-points   (filter #(not (== new-point %)) points)]
      (recur rem-points (dec number-to-pick) (conj picked new-point)))))


(sm/defn pick-medoids :- [s/Any]
  "randomly pick medoids from the *list* of points"
  [points :- [s/Any]
   number-to-pick :- s/Int]
  (pick-medoids-h points number-to-pick []))

(defn get-location-maximum
  "return the point where function achieves its maximum"
  [points func-to-maximize]
  (->> points
       (map func-to-maximize)         ; compute function
       (map-indexed list)             ; add indices
       (apply max-key second)         ; find largest paired with index
       first                          ; just keep the index
       (nth points)))                 ; get the point

(defn closest-medoid
  "find the medoid closest to the point using the similarity function"
  [point medoids similarity]
  (if (some #{point} medoids)
    point ; if the point is a medoid then it is most similar to itself
    (get-location-maximum medoids (partial similarity/get-sim similarity point))))

(defn map-choice
  "check that in point-map for every point the point is in the value
   of the map at that point"
  [points point-map]
  (loop [pt          (first points)
         rem-pts     (rest points)]
    (let [val  (get point-map pt nil)]
      (if (or (nil? val) (not-any? #{pt} val))
        false
        (if (empty? rem-pts)
          true
          (recur (first rem-pts) (rest rem-pts)))))))

(defn medoid-clusters-h
  "computes the collection of clusters for the given medoids (pt
   belongs to closest medoid)"
  [{:keys [points medoids similarity
  clumap]}]
  (if (empty? points)
    clumap
    (let [[point & rem-points]   points
          cluster-for-point      (closest-medoid point medoids similarity)
          old-val                (get clumap cluster-for-point [])
          new-val                (conj old-val point)
          new-clumap             (assoc clumap cluster-for-point new-val)]
      (recur {:points rem-points :medoids medoids :similarity similarity :clumap new-clumap}))))

(defn pairwise-disjoint
  "check that the sets are pairwise disjoint"
  [list-of-sets]
  (if (== (count list-of-sets) 1)
    true
    (->> (combinat/combinations list-of-sets 2)
         (map #(set/intersection (first %) (second %)))
         (map empty?)
         (reduce (fn [x y] (and x y))))))

(defn medoid-clusters
  "compute the cluster partition of points determined by medoids and similarity"
  [similarity points medoids]
  {:pre [(set/subset? (into #{} medoids) (into #{} points))]    ; the medoids are among the points
   :post [(= (apply set/union (map set %)) (into #{} points))   ; the union of the clusters gives all pts
          (pairwise-disjoint (map set %))]}                     ; the parts of the clustering are disjoint

  (let [double              (fn [x] (list x [x]))
        add-pair            (fn [m x] (assoc m (first x) (second x)))
        clumap              (->> medoids
                                 (map double)
                                 (reduce add-pair {}))
        non-medoid-points   (filter #(not (contains? (set medoids) %)) points)
        clumap-result       (medoid-clusters-h {:points non-medoid-points
                                                :medoids medoids
                                                :similarity similarity
                                                :clumap clumap})]
    (assert (map-choice medoids clumap-result))    ; check that the medoids are in their corresponding parts
    (vals clumap-result)))

(defn sum-of-similarities
  "compute the cost of using point as medoid for the given cluster and similarity"
  [{:keys [point cluster similarity]}]
  {:post [(>= % 0)]}
  (->> (filter #(not (== point %)) cluster)
       (map (partial similarity/get-sim similarity point))
       (apply +)))

(defn find-medoid
  "find the medoid of the given cluster using the given similarity map"
  [{:keys [cluster similarity]}]
  {:post [(some #{%} cluster)]}     ; the medoid belongs to the cluster
  (let [sos      (fn [pt] (sum-of-similarities {:point pt :cluster cluster :similarity similarity}))]
    (get-location-maximum cluster sos)))

(defn set-choice
  "the points-list is a choice function for set-list"
  [point-list set-list]
  (and (= (count point-list) (count set-list))
       (->> (map list point-list set-list)
            (map (fn [pair] (some #{(first pair)} (second pair))))
            (map nil?)
            (reduce (fn [x y] (or x y)))
            not)))

(defn find-new-medoids
  "for the clustering find in each cluster the best medoid"
  [similarity clusters]
  {:post [(set-choice % clusters)]}
  (letfn [(f-medoid [cluster] (find-medoid {:cluster cluster :similarity similarity}))]
    (map f-medoid clusters)))

(sm/defn k-medoid-cluster :- #{s/Any}
  [similarity :- (s/protocol similarity/ISimilarity)
   medoid-number :- s/Int
   iteration-no :- s/Int]
  (let [points                (similarity/points similarity)
        initial-medoids       (pick-medoids (seq points) medoid-number)
        clusters->medoids     (partial find-new-medoids similarity)
        medoids->clusters     (partial medoid-clusters similarity points)]
    (loop [medoids           initial-medoids
           iterations-left   iteration-no]
      (trace medoids)
      (if (zero? iterations-left)
        (set medoids)
        (let [clusters             (medoids->clusters medoids)
              new-medoids          (clusters->medoids clusters)]
          (recur new-medoids (dec iterations-left)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Public

;; extract the data from the clustering map and call the correct clustering

(defmethod cluster :k-medoid [{:keys [similarity medoid-number iteration-no]}]
  (k-medoid-cluster similarity medoid-number iteration-no))
