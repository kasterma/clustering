(ns cluster.similarity
  "Similarity for clustering.

   In this file we introduce the ISimilarity protocol and helper
   functions to work with it effectively.  As a user of this
   clustering library the input provided should be an implementation
   of ISimilarity."
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [taoensso.timbre
             :as timbre
             :refer (trace debug info warn error fatal spy)]))

(defprotocol ISimilarity
  (get-sim [_ pt1 pt2])
  (get-sim-pt-set [_ pt set])
  (get-sim-set-set [_ set1 set2])
  (points [_]))

;;; max-max-similarity represents a similarity structure that is
;;; derived from a set of points with a pointwise similarity measure.
;;; The similarity between points and sets, and between sets and sets
;;; is derived from this by taking the (pairwise) maximum value.

(defn max-max-similarity [set-o-points pt-pt-similarity]
  (reify ISimilarity
    (get-sim [_ pt1 pt2]
      (pt-pt-similarity pt1 pt2))

    (get-sim-pt-set [this pt set]
      (apply max (map (partial get-sim this pt) set)))

    (get-sim-set-set [this set1 set2]
      (apply max (map #(get-sim-pt-set this % set2) set1)))

    (points [_]
      set-o-points)))

;;; set-of-numbers represents a similarity structure of a set of
;;; points on the real line, with similarity derived from the distance
;;; (points close to each other are similar).

(defn set-of-numbers [set-o-nums]
  (let [pairs    (combinat/combinations set-o-nums 2)
        dists    (map (fn [[x y]] (Math/abs (- x y))) pairs)
        max-val  (apply max dists)
        pt-pt-sim  (fn [x y] (- max-val (Math/abs (- x y))))]
    (max-max-similarity set-o-nums pt-pt-sim)))
