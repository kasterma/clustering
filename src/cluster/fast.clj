(ns cluster.fast
  "Implementation of Fast clustering

   This clustering optimizes for modularity.  It is the clustering
   described in 'Fast unfolding of communities in large networks' by
   Blonder et al.

   Some key computations are found in clustering.measures where
   modularity is computed.  Here the code driving the clustering is
   given."
  (:use [cluster.core])
  (:require [cluster.graphexamples :as graph]
            [cluster.measure :as meas]
            [taoensso.timbre
             :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]))


(defn fast-clustering-iteration
  "this function performs one iteration of the fast-clustering.

  Note: calling fast-clustering-iteration with nodes not the whole set
  of nodes of a graph will take a limited number of steps.  If nodes
  contains elements that are not (or no longer) vertices of the graph,
  this should correspond to a nop."
  [mclusterable nodes]
  (timbre/info :fast-clustering-iteration
               (count nodes) "to go in this iteration")
  (if (empty? nodes)
    mclusterable
    (recur (meas/takestep mclusterable (first nodes)) (rest nodes))))

(defn drive-fast-clustering
  [mclusterable]
  (loop [start-nodes-count    (count (graph/vertices mclusterable))
         mclust               mclusterable]
    (let [iteration-result    (fast-clustering-iteration
                               mclust
                               (graph/vertices mclust))
          result-count        (count (graph/vertices iteration-result))]
      (if (or (< result-count 2)
              (= result-count start-nodes-count))
        (do
          (timbre/info :drive-fast-clustering
                       "done with driving fast clustering")
          (:clustermap iteration-result))
        (do
          (timbre/info :drive-fast-clustring
                       "starting next iteration with count:" result-count)
          (recur result-count iteration-result))))))

(defmethod cluster :fast [{:keys [mclusterable]}]
  (if (nil? mclusterable)
    (timbre/warn :fast-clustering "this method requires a MatrixClusterable")
    (do
      (timbre/info :fast-clustering "starting")
      (let [clustermap (drive-fast-clustering mclusterable)]
         (spit "~/tmp/clustermap.edn" (str clustermap))))))
