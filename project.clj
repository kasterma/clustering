(defproject cluster "0.1.0-SNAPSHOT"
  :description "Work in progress: Clustering Library."
  :url "https://github.com/kasterma/clustering"
  :aot [cluster.graphexamples]
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/data.json "0.2.3"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [org.clojure/tools.cli "0.2.4"]
                 [midje "1.5.1"]
                 [com.taoensso/timbre "2.6.3"]
                 [incanter "1.5.4"]
                 [net.mikera/vectorz-clj "0.9.0"]
                 [org.clojure/tools.cli "0.2.4"]
                 [org.clojure/core.logic "0.8.4"]
                 [net.mikera/core.matrix "0.7.0"]
                 [org.clojure/algo.monads "0.1.4"]
                 [instaparse "1.2.4"]
                 [prismatic/schema "0.1.6"]]
  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]])
